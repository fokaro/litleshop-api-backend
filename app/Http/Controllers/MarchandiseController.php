<?php

namespace App\Http\Controllers;
use App\Marchandise;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Transactions;


class MarchandiseController extends BaseController
{
    public function getAll(){
 /* $marchandise = DB::table('marchandises')->get(); */
        $marchandise = Marchandise::all();
//$posts1 = $query->get()->reverse();
         //$marchandise = Marchandise:: get()->reverse();
       
        return $marchandise;
    }
    

   public function createMarchandise(Request $request){
   /*  $article = new Marchandise;
    $input = $request->all();
   $article->name = $input->name;
    $article->unitPrice = $input->unitPrice;
    $article->quantity = $input->quantity;
    $article->datePeremption = $input->datePeremption; 
     $article->save(); */
      $mot = marchandise::where('name', $request->get('name'))->first();
      if($mot ==null){

      
     $mot = new Marchandise;

     $mot->name = $request->get('name');
     $mot->unitPrice = $request->get('unit_price');
     $mot->quantity = $request->get('quantity');
     $mot->lastPrice = $request->get('lastPrice');
     $mot->quantitySold = $request->get('quantitySold');

  $mot->save();
  return true;
}else{ return false;
}

   }

   public function getOne(Request $request){

$key =  $request->id;
$marchandise = Marchandise::find($key);

    return $marchandise;

   }

   public function delete(Request $request){
 $id = $request->id;
$marchandise = Marchandise::find($id);
$marchandise->delete();
return $marchandise;
}

 /* public function updateMarchandise(Request $request){
   $id = $request->id;
   $marchandise = Marchandise::find($id);
   $marchandise->name = $request->input('name');
     $marchandise->unitPrice = $request->input('unitPrice');
     $marchandise->quantity = $request->input('quantity');
     $marchandise->lastPrice = $request->input('lastPrice');
     $marchandise->quantitySold = $request->input('quantitySold');


    // $marchandise->totalPrice = $request->input('totalPrice'); 
     $marchandise->save();
return $marchandise;

}  */

public function updateMarchandise(Request $request)
    {
        $id = $request->id;
        $marchandise = Marchandise::find($id);
        if($marchandise != null){
            $marchandise->name = $request->input('name');
            $marchandise->unitPrice = $request->input('unitPrice');

            if($request->input('quantity') < $marchandise->quantity){
                $transactions = new Transactions;
                $transactions->marchandise_name = $marchandise->name;
                $transactions->quantity = $marchandise->quantity - $request->input('quantity');
                $transactions->save();
            }

            $marchandise->quantity = $request->input('quantity');
            $marchandise->lastPrice = $request->input('lastPrice');
            $marchandise->quantitySold = $request->input('quantitySold');

            // $marchandise->totalPrice = $request->input('totalPrice');
            $marchandise->save();
            return $marchandise;
        }
        return null;

    }
public function transactions(Request $request)
{
    $transactions = Transactions::all();

            return $transactions;

}
  
   
}